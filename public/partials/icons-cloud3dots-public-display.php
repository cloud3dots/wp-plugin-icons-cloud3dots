<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://cloud3dots.com
 * @since      0.1.0
 *
 * @package    Icons_Cloud3dots
 * @subpackage Icons_Cloud3dots/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
