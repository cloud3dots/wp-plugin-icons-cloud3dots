<?php

/**
 * Fired during plugin activation
 *
 * @link       https://cloud3dots.com
 * @since      0.1.0
 *
 * @package    Icons_Cloud3dots
 * @subpackage Icons_Cloud3dots/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      0.1.0
 * @package    Icons_Cloud3dots
 * @subpackage Icons_Cloud3dots/includes
 * @author     cloud3dots <cloud3dots@gmail.com>
 */
class Icons_Cloud3dots_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.1.0
	 */
	public static function activate() {

	}

}
