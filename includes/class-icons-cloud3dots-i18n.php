<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://cloud3dots.com
 * @since      0.1.0
 *
 * @package    Icons_Cloud3dots
 * @subpackage Icons_Cloud3dots/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      0.1.0
 * @package    Icons_Cloud3dots
 * @subpackage Icons_Cloud3dots/includes
 * @author     cloud3dots <cloud3dots@gmail.com>
 */
class Icons_Cloud3dots_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    0.1.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'icons-cloud3dots',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
