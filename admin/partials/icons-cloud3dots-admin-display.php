<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://cloud3dots.com
 * @since      0.1.0
 *
 * @package    Icons_Cloud3dots
 * @subpackage Icons_Cloud3dots/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
